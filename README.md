# Building and uploading a deb

1. Make whatever edit or series of edits you want in the project

2. `git add` and `git commit` your edit/s

3. (strongly recommended) `git pull`, to ensure you're up-to-date

3. When you're ready to do a release, run `make build`.  This will
   automatically bump the "local" version number in ROOT/DEBIAN/control from e.g.
   1.0-ch3 to 1.0-ch4. (See [version number conventions](#version-convention))
   
   - `make build` will automatically increment the version number if any file
      in the ROOT/ dir has been modified more recently that the control file.
      If you need to edit the control file (e.g. changing package dependency)
      you'll have to make sure you change the version yourself
   -  If you need to change the version number before the hyphen, do it yourself.

4. Run `make upload` to copy the built deb to our repository. You should then see
   a message like

```
============  Have you pushed to git?? ============
You are 2 commit(s) ahead of upstream
List of uncommitted changes:
(output of a git status command)
===================================================

============  Please consider tagging!! ============
current tag is 1.0-ch5
after pushing your changes, run:
make gittag
to tag this as version 1.0-ch6 and pust the tag to origin
====================================================

```

5. The uncommitted changes listed in the previous step should include
   ROOT/DEBIAN/control and possibly one or more files named .upload-$distro.
   `git add` those, and either
   - git commit -m "Release version 1.0-chX" # Add a new commit, or...
   - git commit --amend # ...squash changes into your previous commit

6. `git push` to push changes back to origin

7. `make gittag` to tag the last commit with the current deb version

# <a name="version-convention"></a>A note on local deb version number conventions

Our local deb packages should use a version number of the form 1.2-ch5. 

For purely local projects, we tend to leave the "1.2" part fixed and increment
the "ch5" part (with the number before the hyphen often being permanently 1.0
in practice!)

We do have a handful of debs which are just locally-repackaged versions of some
3rd-party project: for those, we try to make sure that the part before the
hyphen is the version number of the upstream project.

# Bootstrapping a new project

Ensure that this makedeb repo is a sibling to your other repos, i.e.

```
+ makedeb
+ repo1
  \- <contents of repo1>
+ repo2
  \- <contents of repo2>
```

and then in repo1, repo2 etc add a symlink:
```
ln -s ../makedeb/Makefile ./Makefile
```

(N.B. I considered git submodules, but submodules have to live in a
subdir of the parent repo which didn't seem a good fit)

Once the symlink is in place, you can bootstrap a new layout with "make skel".
Then edit ROOT/DEBIAN/control and touch dists/jessie, dists/xenial etc as
desired
